import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Container, Row, Col, Image } from 'react-bootstrap';

class ForecastResult extends Component {
    render() {
        return (
            <Container fluid style={{ background: 'white', margin: '30px', borderRadius: '15px' }}>

                <Row style={{
                    minHeight:'100px'
                }}>
                    <Col xs={5}>
                    {this.props.forecast.current !== undefined &&
                        <Image src={this.props.forecast.current.weather_icons[0]}
                            alt={this.props.forecast.current.weather_descriptions[0]}
                            style={{ margin: '15px' }} />}

                    </Col>
                    <Col xs={7}>
                        <p style={{
                            fontFamily: 'Montserrat',
                            fontSize: '22px',
                            color: '#404491',
                            margin: '15px'
                        }}>
                            {this.props.forecast.current !== undefined &&
                                <span>{this.props.forecast.current.weather_descriptions}</span>}</p>

                    </Col>
                </Row>
                <hr />
                <Row>
                    <Col>
                        <p style={{
                            fontFamily: 'Montserrat',
                            fontSize: '64px',
                            color: '#404491',
                        }}>{this.props.forecast.current !== undefined &&
                            <span>{this.props.forecast.current.temperature} °C</span>}</p>

                    </Col>
                </Row>
                <Container fluid style={{
                    background: '#404491',
                    borderRadius: '5px',
                    color: 'white',
                    minHeight: '35px',
                    padding:'10px'
                    }}>
                    <Row>
                        <Col>
                            <div><Image src="./Wind.png" />
                                {this.props.forecast.current !== undefined &&
                                    <span>{this.props.forecast.current.wind_speed} km/h</span>}
                            </div>

                        </Col>
                        <Col>
                            <div><Image src="./RainSmall.png" />
                                {this.props.forecast.current !== undefined &&
                                    <span>{this.props.forecast.current.humidity} %</span>}
                            </div>

                        </Col>
                    </Row>

                </Container>

            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        forecast: state.forecast.forecast
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult);
