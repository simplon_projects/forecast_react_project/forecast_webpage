import React, { Component } from 'react';
import Forecast from './Forecast';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  render() {
    return (
    <div className="App">
      <header className="App-header">
        <div>
          <Forecast />
        </div>
      </header>
    </div>
  );
  }
  
}

export default App;
