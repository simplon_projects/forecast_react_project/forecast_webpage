import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { InputGroup, FormControl, Button, Container, Row, Col, Spinner, Image } from 'react-bootstrap';
import { updateCity } from '../actions/forecast';

class ForecastChooseCity extends Component {
    render() {
        return (
            <Container style={{ margin: '20px'}}>
                <Row>
                    <Col>
                        <InputGroup className="mb-3" style={{ width:'300px'}}>
                            <FormControl
                                placeholder="Choose a city"
                                value={this.props.city}
                                onChange={(event) => { this.props.updateCity(event.target.value) }}
                            />
                            <InputGroup.Append >
                                <Button variant="outline-dark" onClick={this.props.onClick}>
                                    <Image src="./search.png" style={{ width:'25px', height:'25px'}} />
                                </Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {this.props.loader &&
                            <Spinner animation="border" role="status">
                                <span className="sr-only">Loading...</span>
                            </Spinner>
                        }
                    </Col>
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        city: state.forecast.city,
        loader: state.forecast.loader
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateCity
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastChooseCity);