import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchForecast } from '../actions/forecast';
import ForecastChooseCity from './ForecastChooseCity';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';
import { Container, Card, Row } from 'react-bootstrap';


class Forecast extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <ForecastTitle city={this.props.city} />
                </Row>
                <Row>
                    < ForecastResult />
                </Row>
                <Row>
                    <ForecastChooseCity onClick={() => { this.props.fetchForecast(this.props.city) }} />
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        forecast: state.forecast.forecast,
        city: state.forecast.city
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchForecast,
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Forecast);
