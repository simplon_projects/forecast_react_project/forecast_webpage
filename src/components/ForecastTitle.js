import React, { Component } from 'react';
import { Container, Row } from 'react-bootstrap';

class ForecastTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date().toLocaleDateString('fr-fr', { weekday: 'short', day: 'numeric', month: 'short' }),
        }
    }
    render() {
        return (
            <Container >
                <Row>
                    <h1
                        style={{
                            fontFamily: 'Montserrat',
                            fontSize: '62px',
                            color: 'white',
                            margin:'auto'
                        }}
                    >{this.props.city}</h1>
                </Row>
                <Row>
                    <p
                        style={{
                            fontFamily: 'Montserrat',
                            fontSize: '22px',
                            color: 'white',
                            margin:'auto'
                        }}>{this.state.date}</p>
                </Row>
            </Container>

        )
    }
}
export default ForecastTitle;